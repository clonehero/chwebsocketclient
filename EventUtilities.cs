
namespace ChWebSocketClient
{
    public static class EventUtilities
    {

        public static string EventTypeToName(EventTypes eventType)
        {
            switch (eventType)
            {
                case EventTypes.EventError: return "event_error";
                case EventTypes.ClientConfig: return "client_config";
                case EventTypes.PresenceUpdate: return "presence_update";
                case EventTypes.ProfileUpdate: return "profile_update";
                case EventTypes.SongMetadata: return "song_metadata";
                case EventTypes.GameplayUpdate: return "gameplay_update";
                case EventTypes.GameplayLoad: return "gameplay_load";
                case EventTypes.GameplayScoreUpdate: return "gameplay_score_update";
                case EventTypes.GameplayInputUpdate: return "gameplay_input_update";
                case EventTypes.GameplayNoteUpdate: return "gameplay_note_update";
                case EventTypes.GameplayPause: return "gameplay_pause";
                case EventTypes.GameplayResume: return "gameplay_resume";
                case EventTypes.GameplayEnd: return "gameplay_end";
                case EventTypes.GameplaySkipSong: return "gameplay_skip_song";
                case EventTypes.GameplaySectionChange: return "gameplay_section_change";
                case EventTypes.GameplaySPActivation: return "gameplay_sp_activation";
                case EventTypes.GameplayStarEarned: return "gameplay_star_earned";
                case EventTypes.ResultsFinalScore: return "results_final_score";
                case EventTypes.SceneChange: return "scene_change";
                default: return "";
            }
        }

        public static EventTypes EventNameToType(string eventName)
        {
            switch (eventName)
            {
                case "event_error": return EventTypes.EventError;
                case "client_config": return EventTypes.ClientConfig;
                case "presence_update": return EventTypes.PresenceUpdate;
                case "profile_update": return EventTypes.ProfileUpdate;
                case "song_metadata": return EventTypes.SongMetadata;
                case "gameplay_update": return EventTypes.GameplayUpdate;
                case "gameplay_load": return EventTypes.GameplayLoad;
                case "gameplay_score_update": return EventTypes.GameplayScoreUpdate;
                case "gameplay_input_update": return EventTypes.GameplayInputUpdate;
                case "gameplay_note_update": return EventTypes.GameplayNoteUpdate;
                case "gameplay_pause": return EventTypes.GameplayPause;
                case "gameplay_resume": return EventTypes.GameplayResume;
                case "gameplay_end": return EventTypes.GameplayEnd;
                case "gameplay_skip_song": return EventTypes.GameplaySkipSong;
                case "gameplay_section_change": return EventTypes.GameplaySectionChange;
                case "gameplay_sp_activation": return EventTypes.GameplaySPActivation;
                case "gameplay_star_earned": return EventTypes.GameplayStarEarned;
                case "results_final_score": return EventTypes.ResultsFinalScore;
                case "scene_change": return EventTypes.SceneChange;
                default: return EventTypes.None;
            }
        }

        public static IEvent EventFactory(EventTypes eventType)
        {
            switch (eventType)
            {
                case EventTypes.ClientConfig: return new ClientConfigEvent();
                // case EventTypes.PresenceUpdate: return null;
                case EventTypes.ProfileUpdate: return new ProfileUpdateEvent();
                case EventTypes.SongMetadata: return new SongMetadataEvent();
                case EventTypes.GameplayUpdate: return new GameplayUpdateEvent();
                case EventTypes.GameplayLoad: return new GameplayLoadEvent();
                // case EventTypes.GameplayScoreUpdate: return new ;
                // case EventTypes.GameplayInputUpdate: return new ;
                // case EventTypes.GameplayNoteUpdate: return new ;
                case EventTypes.GameplayPause: return new GameplayPauseEvent();
                case EventTypes.GameplayResume: return new GameplayPauseEvent();
                // case EventTypes.GameplayEnd: return new ;
                // case EventTypes.GameplaySkipSong: return new ;
                // case EventTypes.GameplaySectionChange: return new ;
                // case EventTypes.GameplaySPActivation: return new ;
                case EventTypes.GameplayStarEarned: return new GameplayStarEarnedEvent();
                // case EventTypes.ResultsFinalScore: return new ;
                case EventTypes.SceneChange: return new SceneChangeEvent();
                default: return null;
            }
        }
    }
}