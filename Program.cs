﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ChWebSocketClient
{
    public class Program
    {
        static int clientCount = 10;
        static async Task Main(string[] args)
        {
            List<Task> tasks = new List<Task>();

            Console.WriteLine($"Spawning {clientCount} clients");
            for (int i = 0; i < clientCount; ++i)
            {
                // await Task.Delay(1);
                tasks.Add(NewClient(i));
            }
            await Task.WhenAll(tasks);
        }

        static async Task NewClient(int i)
        {
            try
            {
                WebsocketClient ws = new("ws://127.0.0.1:12345");

                while (true)
                {
                    try
                    {
                        await ws.Connect();
                        break;
                    }
                    catch (Exception)
                    {

                    }
                }

                while (true)
                {
                    IEvent ev = await ws.GetNextEvent();

                    if (ev == null)
                    {
                        if (!ws.IsConnected)
                        {
                            break;
                        }
                        continue;
                    }

                    switch (ev.EventType)
                    {
                        case EventTypes.GameplayScoreUpdate:
                            GameplayScoreEvent scoreEvent = (GameplayScoreEvent)ev;
                            if (scoreEvent.Score == 999999)
                            {
                                Console.WriteLine($"{i} Score: {scoreEvent.Score}");
                                return;
                            }
                            break;
                        default:
                            Console.WriteLine($"Event {ev.EventType} recieved!");
                            break;
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
        }
    }
}