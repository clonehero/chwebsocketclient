using System;
using System.Net.WebSockets;
using System.Threading.Tasks;
using System.Threading;
using System.Text.Json;
using System.Buffers;
using System.Collections.Generic;
using System.Collections.Concurrent;
using FastEnumUtility;

namespace ChWebSocketClient
{
    public class WebsocketClient : IDisposable
    {
        ClientWebSocket client;
        string connectionString;
        Utf8JsonWriter jsonWriter;
        ArrayBufferWriter<byte> writeBuffer;
        ArrayBufferWriter<byte> receiveBuffer;
        bool isConnected = false;
        public bool IsConnected { get => isConnected; }

        public WebsocketClient(string connectionString)
        {
            client = new ClientWebSocket();
            this.connectionString = connectionString;
            writeBuffer = new ArrayBufferWriter<byte>();
            receiveBuffer = new ArrayBufferWriter<byte>();
            jsonWriter = new Utf8JsonWriter(writeBuffer);
        }

        public async Task Connect()
        {
            Console.WriteLine("Connecting to server");
            await client.ConnectAsync(new Uri(connectionString), CancellationToken.None);

            // sent connection payload
            ClientConfigEvent clientConfigEvent = new ClientConfigEvent();
            clientConfigEvent.ClientName = "Example client";
            clientConfigEvent.UpdateInterval = 20;
            clientConfigEvent.EventSubscriptions = EventTypes.GameplayScoreUpdate;

            writeBuffer.Clear();
            jsonWriter.Reset();
            jsonWriter.WriteStartObject();
            jsonWriter.WritePropertyName(EventUtilities.EventTypeToName(EventTypes.ClientConfig));
            jsonWriter.WriteStartObject();
            clientConfigEvent.SerializeJSON(jsonWriter);
            jsonWriter.WriteEndObject();
            jsonWriter.WriteEndObject();
            jsonWriter.Flush();

            Console.WriteLine("Sending connection payload");

            await client.SendAsync(writeBuffer.WrittenMemory, WebSocketMessageType.Text, true, CancellationToken.None);
            isConnected = true;
            _ = CheckForData();// Start data download task
        }

        private ConcurrentQueue<IEvent> activeEvents = new ConcurrentQueue<IEvent>();

        private void ReadJson()
        {
            // Console.WriteLine(System.Text.Encoding.UTF8.GetString(receiveBuffer.WrittenSpan));
            Utf8JsonReader json = new Utf8JsonReader(receiveBuffer.WrittenSpan);

            json.ReadObjectStart();

            while (json.Read())
            {
                if (json.TokenType == JsonTokenType.EndObject)
                {
                    break;
                }

                var eventType = json.PeekPropertyName();
                EventTypes type = EventUtilities.EventNameToType(eventType);

                // Read array of events
                json.ReadArrayStart();

                while (json.Read())
                {
                    if (json.TokenType == JsonTokenType.EndArray)
                    {
                        break;
                    }
                    var ev = EventUtilities.EventFactory(type);

                    if (ev != null)
                    {
                        json.ValidateObjectStart();
                        ev.DeserializeJSON(ref json);
                        json.ReadObjectEnd();
                        activeEvents.Enqueue(ev);
                    }
                    else if (json.TrySkip())
                    {
                        Console.WriteLine("Skipped unimplemented type");
                    }
                    else
                    {
                        JsonReadUtils.ThrowJsonException("Unknown event type found, and failed to skip the data");
                    }
                };
            }
        }

        private async Task CheckForData()
        {
            while (isConnected)
            {
                try
                {
                    receiveBuffer.Clear();
                    ValueWebSocketReceiveResult result;
                    do
                    {
                        // Console.WriteLine("TEST!");
                        result = await client.ReceiveAsync(receiveBuffer.GetMemory(), CancellationToken.None);
                        receiveBuffer.Advance(result.Count);
                    }
                    while (result.EndOfMessage != true);
                    ReadJson();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                }
                if (client.State != WebSocketState.Open)
                {
                    isConnected = false;
                    Console.WriteLine("STOPPING DATA!!!");
                }
            }
        }

        public async Task<IEvent> GetNextEvent()
        {
            while (activeEvents.IsEmpty)
            {
                await Task.Delay(2);
            }

            if (activeEvents.TryDequeue(out IEvent ev))
            {
                return ev;
            }
            else
            {
                return null;
            }
        }

        public void Dispose()
        {
            client.Dispose();
        }
    }
}