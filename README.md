ChWebSocketClient
=================

**Note** This has been abandoned for CH, as we have decided to push websocket support to strikeline. This feature will be revived at a later point in time, but we have decided to not to merge the implementation into ch v1 due to time constraints and major bugs that we need to work out.

~~This repo contains a C# websocket client implementation for the upcoming Clone Hero websocket api.

The websocket api will allow anyone to write code to receive events directly from the game.

These events will enable many use cases such as:
* Custom Dynamic Stream overlays
* Reacting to notes being hit or missed
* Reacting to controller button inputs
* Score tracking

In general the game server code is very high performance, so if you are going to be writing code to interface with the game it will need to be also. If your client cannot recieve events fast enough then make sure you set the event filters, and send rate properly in your initial connection json payload.

This repo will also generally surve as a place to document the JSON events and websocket api overall once it is more finalized.

API Suggestions
-----------
If you have any suggestions for the websocket api please make an issue for it on this repo. Currently the planned event types are located in EventTypes.cs not all of these have been implemented into the game yet. As events are implemented in the game i will be adding/updating the events here as well. ~~
