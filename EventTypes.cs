using System;

namespace ChWebSocketClient
{
    [Flags]
    public enum EventTypes
    {
        None = 0,
        EventError = 1,
        ClientConfig = 2,
        PresenceUpdate = 4,
        ProfileUpdate = 8,
        SongMetadata = 16,
        GameplayUpdate = 32,
        GameplayScoreUpdate = 64,
        GameplayInputUpdate = 128,
        GameplayNoteUpdate = 256,
        GameplaySectionChange = 512,
        GameplaySPActivation = 1024,
        GameplaySPEarned = 2048,
        GameplayStarEarned = 4096,
        GameplayPause = 8192,
        GameplayResume = 16384,
        GameplayEnd = 32768,
        GameplaySkipSong = 65536,
        ResultsFinalScore = 131072,
        SceneChange = 262144,
        GameplayLoad = 524288,
    }
}