
using System.Text.Json;

namespace ChWebSocketClient
{
    public class SceneChangeEvent : IEvent
    {
        public EventTypes EventType => EventTypes.SceneChange;

        public string SceneName;

        public void DeserializeJSON(ref Utf8JsonReader json)
        {
            SceneName = json.ReadString("scene_name");
        }

        public void SerializeJSON(Utf8JsonWriter json)
        {
            json.WriteString("scene_name", SceneName);
        }
    }
}