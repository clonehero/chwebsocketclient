using System.Text.Json;

namespace ChWebSocketClient
{

    public class GameplayUpdateEvent : IEvent
    {
        public EventTypes EventType => EventTypes.GameplayUpdate;

        public float FrameTimeDelta;

        public double SongTime;
        public double SongTimeVideoCal;

        public double LastSongTime;
        public double SongTimeDelta;

        public double AudioTime;

        public uint SongTickPosition;
        public uint LastSongTickPosition;
        public int TicksPerEightMeasures;
        public bool IsResuming;
        public bool IsPaused;

        public void DeserializeJSON(ref Utf8JsonReader json)
        {
            FrameTimeDelta = json.ReadFloat("frame_time_delta");
            SongTime = json.ReadDouble("song_time");
            SongTimeVideoCal = json.ReadDouble("song_time_video_cal");
            LastSongTime = json.ReadDouble("last_song_time");
            SongTimeDelta = json.ReadDouble("song_time_delta");
            AudioTime = json.ReadDouble("audio_time");
            SongTickPosition = json.ReadUInt32("song_tick_position");
            LastSongTickPosition = json.ReadUInt32("last_song_tick_position");
            TicksPerEightMeasures = json.ReadInt32("ticks_per_eight_measures");
            IsResuming = json.ReadBool("is_resuming");
            IsPaused = json.ReadBool("is_paused");
        }

        public void SerializeJSON(Utf8JsonWriter json)
        {
            json.WriteNumber("frame_time_delta", FrameTimeDelta);
            json.WriteNumber("song_time", SongTime);
            json.WriteNumber("song_time_video_cal", SongTimeVideoCal);
            json.WriteNumber("last_song_time", LastSongTime);
            json.WriteNumber("song_time_delta", SongTimeDelta);
            json.WriteNumber("audio_time", AudioTime);
            json.WriteNumber("song_tick_position", SongTickPosition);
            json.WriteNumber("last_song_tick_position", LastSongTickPosition);
            json.WriteNumber("ticks_per_eight_measures", TicksPerEightMeasures);
            json.WriteBoolean("is_resuming", IsResuming);
            json.WriteBoolean("is_paused", IsPaused);
        }
    }
}