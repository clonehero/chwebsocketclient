using System;
using System.Text.Json;

namespace ChWebSocketClient
{
    public class SongMetadataEvent : IEvent
    {
        public EventTypes EventType => EventTypes.SongMetadata;

        public string Name;
        public string Artist;
        public string Album;
        public string Genre;
        public string Charter;
        public string Year;
        public string Playlist;

        public string Checksum;
        public string SongPath;
        public string IconName;

        public int Offset;
        public int VideoStart;
        public int PreviewStart;
        public int SongLength;
        public int CustomHOPO;
        public int MultiplierNote;

        public short PlaylistTrack;
        public short AlbumTrack;
        public ushort PlayCount;
        public DateTime DateAdded;
        public bool IsMIDIChart;
        public bool HasVideoBackground;
        public bool IsAvailableOnline;
        public bool HasLyrics;
        public bool IsModchart;
        public bool ForceProDrums;
        public bool IsEighthHOPO;

        public sbyte GuitarIntensity;
        public sbyte GuitarCoopIntensity;
        public sbyte BassIntensity;
        public sbyte RhythmIntensity;
        public sbyte DrumsIntensity;
        public sbyte ProDrumsIntensity;
        public sbyte BandIntensity;
        public sbyte KeysIntensity;
        public sbyte SixfretGuitarIntensity;
        public sbyte SixfretBassIntensity;

        public bool IsStartingSong;

        public void DeserializeJSON(ref Utf8JsonReader json)
        {
            Name = json.ReadString("name");
            Artist = json.ReadString("artist");
            Album = json.ReadString("album");
            Genre = json.ReadString("genre");
            Charter = json.ReadString("charter");
            Year = json.ReadString("year");
            Playlist = json.ReadString("playlist");
            Checksum = json.ReadString("checksum");
            SongPath = json.ReadString("song_path");
            IconName = json.ReadString("icon_name");
            Offset = json.ReadInt32("offset");
            VideoStart = json.ReadInt32("video_start");
            PreviewStart = json.ReadInt32("preview_start");
            SongLength = json.ReadInt32("song_length");
            CustomHOPO = json.ReadInt32("custom_hopo");
            MultiplierNote = json.ReadInt32("multiplier_note");

            PlaylistTrack = json.ReadInt16("playlist_track");
            AlbumTrack = json.ReadInt16("album_track");
            PlayCount = json.ReadUInt16("play_count");

            DateAdded = json.ReadDateTime("date_added");

            IsMIDIChart = json.ReadBool("is_midi_chart");
            HasVideoBackground = json.ReadBool("has_video_background");
            IsAvailableOnline = json.ReadBool("is_available_online");
            HasLyrics = json.ReadBool("has_lyrics");
            IsModchart = json.ReadBool("is_modchart");
            ForceProDrums = json.ReadBool("force_prodrums");
            IsEighthHOPO = json.ReadBool("is_eighth_hopo");

            GuitarIntensity = json.ReadSByte("guitar_difficulty");
            GuitarCoopIntensity = json.ReadSByte("guitar_coop_difficulty");
            BassIntensity = json.ReadSByte("bass_difficulty");
            RhythmIntensity = json.ReadSByte("rhythm_difficulty");
            DrumsIntensity = json.ReadSByte("drums_difficulty");
            ProDrumsIntensity = json.ReadSByte("prodrums_difficulty");
            BandIntensity = json.ReadSByte("band_difficulty");
            KeysIntensity = json.ReadSByte("keys_difficulty");
            SixfretGuitarIntensity = json.ReadSByte("sixfret_guitar_difficulty");
            SixfretBassIntensity = json.ReadSByte("sixfret_bass_difficulty");
            IsStartingSong = json.ReadBool("is_starting_song");
        }

        public void SerializeJSON(Utf8JsonWriter json)
        {
            json.WriteString("name", Name);
            json.WriteString("artist", Artist);
            json.WriteString("album", Album);
            json.WriteString("genre", Genre);
            json.WriteString("charter", Charter);
            json.WriteString("year", Year);
            json.WriteString("playlist", Playlist);
            json.WriteString("checksum", Checksum);
            json.WriteString("song_path", SongPath);
            json.WriteString("icon_name", IconName);
            json.WriteNumber("offset", Offset);
            json.WriteNumber("video_start", VideoStart);
            json.WriteNumber("preview_start", PreviewStart);
            json.WriteNumber("song_length", SongLength);
            json.WriteNumber("custom_hopo", CustomHOPO);
            json.WriteNumber("multiplier_note", MultiplierNote);

            json.WriteNumber("playlist_track", PlaylistTrack);
            json.WriteNumber("album_track", AlbumTrack);
            json.WriteNumber("play_count", PlayCount);

            json.WriteString("date_added", DateAdded);

            json.WriteBoolean("is_midi_chart", IsMIDIChart);
            json.WriteBoolean("has_video_background", HasVideoBackground);
            json.WriteBoolean("is_available_online", IsAvailableOnline);
            json.WriteBoolean("has_lyrics", HasLyrics);
            json.WriteBoolean("is_modchart", IsModchart);
            json.WriteBoolean("force_prodrums", ForceProDrums);
            json.WriteBoolean("is_eighth_hopo", IsEighthHOPO);

            json.WriteNumber("guitar_difficulty", GuitarIntensity);
            json.WriteNumber("guitar_coop_difficulty", GuitarCoopIntensity);
            json.WriteNumber("bass_difficulty", BassIntensity);
            json.WriteNumber("rhythm_difficulty", RhythmIntensity);
            json.WriteNumber("drums_difficulty", DrumsIntensity);
            json.WriteNumber("prodrums_difficulty", ProDrumsIntensity);
            json.WriteNumber("band_difficulty", BandIntensity);
            json.WriteNumber("keys_difficulty", KeysIntensity);
            json.WriteNumber("sixfret_guitar_difficulty", SixfretGuitarIntensity);
            json.WriteNumber("sixfret_bass_difficulty", SixfretBassIntensity);
            json.WriteBoolean("is_starting_song", IsStartingSong);
        }
    }
}