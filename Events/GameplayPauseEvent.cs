using System;
using System.Text.Json;

namespace ChWebSocketClient
{
    public class GameplayPauseEvent : IEvent
    {
        public EventTypes EventType => EventTypes.GameplayPause;

        public bool isPaused;
        public float lastPauseTime;

        public void DeserializeJSON(ref Utf8JsonReader json)
        {
            isPaused = json.ReadBool("is_paused");
            lastPauseTime = json.ReadFloat("last_pause_time");
        }

        public void Reset()
        {
            isPaused = default;
            lastPauseTime = default;
        }

        public void SerializeJSON(Utf8JsonWriter json)
        {
            json.WriteBoolean("is_paused", isPaused);
            json.WriteNumber("last_pause_time", lastPauseTime);
        }
    }
}