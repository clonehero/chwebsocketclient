using System;
using System.Text.Json;

namespace ChWebSocketClient
{
    public class GameplayStarEarnedEvent : IEvent
    {
        public EventTypes EventType => EventTypes.GameplayStarEarned;

        public int CurrentStar;

        public void DeserializeJSON(ref Utf8JsonReader json)
        {
            CurrentStar = json.ReadInt32("current_star");
        }

        public void SerializeJSON(Utf8JsonWriter json)
        {
            json.WriteNumber("current_star", CurrentStar);
        }
    }
}