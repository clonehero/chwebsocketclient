using System;
using System.Text.Json;

namespace ChWebSocketClient
{
    public class GameplayNoteEvent : IEvent
    {
        public EventTypes EventType => EventTypes.GameplayNoteUpdate;

        public int NoteID;

        public float Time;
        public float Length;
        public float hitTime;

        public uint TickPosition;
        public int TickLength;

        public ushort Mask;
        //Note state info
        public bool WasHit;
        public bool WasMissed;
        public bool IsDead;
        public bool IsSustaining;
        //Note type info
        public bool IsChord;
        public bool IsDisjoint;
        public bool IsSlave;
        public bool IsStarPower;
        public bool IsStarPowerEnd;
        public bool IsExtendedSustain;
        public bool IsHopo;
        public bool IsPureHopo;
        public bool IsTap;
        public bool IsTom;
        public bool IsSoloBegin;
        public bool IsSoloEnd;
        public bool IsDrumActivation;
        public bool IsGhost;
        public bool IsAccent;

        public void DeserializeJSON(ref Utf8JsonReader json)
        {
            json.ReadObjectStart();
            NoteID = json.ReadInt32("note_id");
            Time = json.ReadFloat("time");
            Length = json.ReadFloat("length");
            hitTime = json.ReadFloat("hit_time");

            TickPosition = json.ReadUInt32("tick_position");
            TickLength = json.ReadInt32("tick_length");

            Mask = json.ReadUInt16("mask");

            IsChord = json.ReadBool("is_chord");
            IsDisjoint = json.ReadBool("is_disjoint");
            IsSlave = json.ReadBool("is_slave");
            IsStarPower = json.ReadBool("is_star_power");
            IsStarPowerEnd = json.ReadBool("is_star_power_end");
            IsExtendedSustain = json.ReadBool("is_extended_sustain");
            IsHopo = json.ReadBool("is_hopo");
            IsPureHopo = json.ReadBool("is_pure_hopo");
            IsTap = json.ReadBool("is_tap");
            IsTom = json.ReadBool("is_tom");
            IsSoloBegin = json.ReadBool("is_solo_begin");
            IsSoloEnd = json.ReadBool("is_solo_end");
            IsDrumActivation = json.ReadBool("is_drum_activation");
            IsGhost = json.ReadBool("is_ghost");
            IsAccent = json.ReadBool("is_accent");

            json.ReadObjectEnd();
        }

        public void SerializeJSON(Utf8JsonWriter json)
        {
            json.WriteNumber("note_id", NoteID);
            json.WriteNumber("time", Time);
            json.WriteNumber("length", Length);
            json.WriteNumber("hit_time", hitTime);

            json.WriteNumber("tick_position", TickPosition);
            json.WriteNumber("tick_length", TickLength);

            json.WriteNumber("mask", Mask);

            json.WriteBoolean("is_chord", IsChord);
            json.WriteBoolean("is_disjoint", IsDisjoint);
            json.WriteBoolean("is_slave", IsSlave);
            json.WriteBoolean("is_star_power", IsStarPower);
            json.WriteBoolean("is_star_power_end", IsStarPowerEnd);
            json.WriteBoolean("is_extended_sustain", IsExtendedSustain);
            json.WriteBoolean("is_hopo", IsHopo);
            json.WriteBoolean("is_pure_hopo", IsPureHopo);
            json.WriteBoolean("is_tap", IsTap);
            json.WriteBoolean("is_tom", IsTom);
            json.WriteBoolean("is_solo_begin", IsSoloBegin);
            json.WriteBoolean("is_solo_end", IsSoloEnd);
            json.WriteBoolean("is_drum_activation", IsDrumActivation);
            json.WriteBoolean("is_ghost", IsGhost);
            json.WriteBoolean("is_accent", IsAccent);
        }
    }
}