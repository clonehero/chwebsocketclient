﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using FastEnumUtility;

namespace ChWebSocketClient
{
    public class GameplayScoreEvent : IEvent
    {
        public EventTypes EventType => EventTypes.GameplayScoreUpdate;

        public double GameTime;
        public int Score;

        //For star status
        public int CurrentStar;
        public float PercentToStarScore;

        public void DeserializeJSON(ref Utf8JsonReader jsonReader)
        {
            Score = jsonReader.ReadInt32("score");
            GameTime = jsonReader.ReadFloat("game_time");
            CurrentStar = jsonReader.ReadInt32("current_star");
            PercentToStarScore = jsonReader.ReadFloat("percent_to_next_star");
        }

        public void SerializeJSON(Utf8JsonWriter jsonWriter)
        {
            jsonWriter.WriteNumber("score", Score);
            jsonWriter.WriteNumber("game_time", GameTime);
            jsonWriter.WriteNumber("current_star", CurrentStar);
            jsonWriter.WriteNumber("percent_to_next_star", PercentToStarScore);
        }
    }
}