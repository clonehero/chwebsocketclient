using System.Collections.Generic;
using System.Text.Json;
using FastEnumUtility;

namespace ChWebSocketClient
{
    public class ClientConfigEvent : IEvent
    {
        public EventTypes EventType => EventTypes.ClientConfig;

        public string ClientName;
        public int UpdateInterval = 10;
        public int MaxEventBatchSize = 32;
        public EventTypes EventSubscriptions = EventTypes.None;

        private List<EventTypes> eventsTmp = new List<EventTypes>();

        public void DeserializeJSON(ref Utf8JsonReader jsonReader)
        {
            ClientName = jsonReader.ReadString("client_name");
            UpdateInterval = jsonReader.ReadInt32("update_interval");
            MaxEventBatchSize = jsonReader.ReadInt32("max_batch");
            jsonReader.ReadEnumArray("event_subscriptions", eventsTmp);
            EventSubscriptions = ConvertEventTypesList();
        }

        private EventTypes ConvertEventTypesList()
        {
            EventTypes evMerge = EventTypes.None;
            foreach (EventTypes ev in eventsTmp)
            {
                evMerge |= ev;
            }
            return evMerge;
        }

        public void SerializeJSON(Utf8JsonWriter jsonWriter)
        {
            jsonWriter.WriteString("client_name", ClientName);
            jsonWriter.WriteNumber("update_interval", UpdateInterval);
            jsonWriter.WriteNumber("max_batch", MaxEventBatchSize);

            jsonWriter.WriteStartArray("event_subscriptions");
            foreach (var ev in BitHelper.EnumerateBitFlags((uint)EventSubscriptions))
            {
                jsonWriter.WriteStringValue(FastEnum.GetName((EventTypes)ev));
            }
            jsonWriter.WriteEndArray();
        }
    }
}