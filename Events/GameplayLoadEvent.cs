using System.Text.Json;
using FastEnumUtility;

namespace ChWebSocketClient
{
    public class GameplayLoadEvent : IEvent
    {
        public EventTypes EventType => EventTypes.GameplayLoad;

        public int TicksPerFullSPBar;
        public int TicksPerPoint;
        public uint TicksPerQuarterBeat;
        public bool IsAnyPlayerBot;
        public int PlayerCount;
        public GameMode GameMode;
        public int SongResolution;

        public bool IsPlayingSetlist;
        public int SetlistLength;
        public int SetlistIndex;
        public bool ScoringDisabled;

        public int SustainDropLeniency;
        public int DoubleStrumPrevention;

        public int VideoOffset;
        public int AudioOffset;

        public bool IsPracticeMode;

        public void DeserializeJSON(ref Utf8JsonReader json)
        {
            TicksPerFullSPBar = json.ReadInt32("ticks_per_full_sp_bar");
            TicksPerPoint = json.ReadInt32("ticks_per_point");
            TicksPerQuarterBeat = json.ReadUInt32("ticks_per_quarter_beat");
            IsAnyPlayerBot = json.ReadBool("is_any_player_bot");
            PlayerCount = json.ReadInt32("player_count");
            GameMode = FastEnum.Parse<GameMode>(json.ReadString("game_mode"));
            SongResolution = json.ReadInt32("song_resolution");
            IsPlayingSetlist = json.ReadBool("is_playing_setlist");
            SetlistLength = json.ReadInt32("setlist_length");
            SetlistIndex = json.ReadInt32("setlist_index");
            ScoringDisabled = json.ReadBool("scoring_disabled"); // TODO add enum?
            SustainDropLeniency = json.ReadInt32("sustain_drop_leniency");
            DoubleStrumPrevention = json.ReadInt32("double_strum_prevention");
            VideoOffset = json.ReadInt32("video_offset");
            AudioOffset = json.ReadInt32("audio_offset");
            IsPracticeMode = json.ReadBool("is_practice_mode");
        }

        public void SerializeJSON(Utf8JsonWriter json)
        {
            json.WriteNumber("ticks_per_full_sp_bar", TicksPerFullSPBar);
            json.WriteNumber("ticks_per_point", TicksPerPoint);
            json.WriteNumber("ticks_per_quarter_beat", TicksPerQuarterBeat);
            json.WriteBoolean("is_any_player_bot", IsAnyPlayerBot);
            json.WriteNumber("player_count", PlayerCount);
            json.WriteString("game_mode", FastEnum.GetName(GameMode));
            json.WriteNumber("song_resolution", SongResolution);
            json.WriteBoolean("is_playing_setlist", IsPlayingSetlist);
            json.WriteNumber("setlist_length", SetlistLength);
            json.WriteNumber("setlist_index", SetlistIndex);
            json.WriteBoolean("scoring_disabled", ScoringDisabled); // TODO add enum?
            json.WriteNumber("sustain_drop_leniency", SustainDropLeniency);
            json.WriteNumber("double_strum_prevention", DoubleStrumPrevention);
            json.WriteNumber("video_offset", VideoOffset);
            json.WriteNumber("audio_offset", AudioOffset);
            json.WriteBoolean("is_practice_mode", IsPracticeMode);
        }
    }
}