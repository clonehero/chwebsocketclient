using System.Collections.Generic;
using System.Text.Json;
using FastEnumUtility;

namespace ChWebSocketClient
{
    public class ProfileUpdateEvent : IEvent
    {
        public EventTypes EventType => EventTypes.ProfileUpdate;

        public string ProfileName;

        public ControllerType ControlType;
        public Instrument CurrentInstrument;
        public Difficulty CurrentDifficulty;
        public Modifier CurrentNoteModifiers;

        public float HighwayLength;
        public int NoteSpeed;
        public bool CanTiltActivate;
        public bool LeftyFlip;
        public bool GamepadMode;
        public bool IsBot;
        public bool ShowName;
        public bool IsGuestProfile;
        public bool IsRemotePlayer;

        private readonly List<Modifier> tmpModifiers = new List<Modifier>();

        public void DeserializeJSON(ref Utf8JsonReader json)
        {
            ProfileName = json.ReadString("profile_name");

            ControlType = json.ReadEnum<ControllerType>("controller_type");
            CurrentInstrument = json.ReadEnum<Instrument>("current_instrument");
            CurrentDifficulty = json.ReadEnum<Difficulty>("current_difficulty");
            tmpModifiers.Clear();
            json.ReadEnumArray<Modifier>("current_note_modifiers", tmpModifiers);
            CurrentNoteModifiers = Modifier.None;
            foreach (var modifier in tmpModifiers)
            {
                CurrentNoteModifiers |= modifier;
            }

            HighwayLength = json.ReadFloat("highway_length");
            NoteSpeed = json.ReadInt32("note_speed");
            CanTiltActivate = json.ReadBool("can_tilt_activate");
            LeftyFlip = json.ReadBool("lefty_flip");
            GamepadMode = json.ReadBool("gamepad_mode");
            IsBot = json.ReadBool("is_bot");
            ShowName = json.ReadBool("show_name");
            IsGuestProfile = json.ReadBool("is_guest_profile");
            IsRemotePlayer = json.ReadBool("is_remote_player");
        }

        public void SerializeJSON(Utf8JsonWriter json)
        {
            json.WriteString("profile_name", ProfileName);
            json.WriteString("controller_type", FastEnum.GetName(ControlType));
            json.WriteString("current_instrument", FastEnum.GetName(CurrentInstrument));
            json.WriteString("current_difficulty", FastEnum.GetName(CurrentDifficulty));

            json.WriteStartArray("current_note_modifiers");
            foreach (var modifier in BitHelper.EnumerateBitFlags((uint)CurrentNoteModifiers))
            {
                json.WriteStringValue(FastEnum.GetName((Modifier)modifier));
            }
            json.WriteEndArray();

            json.WriteNumber("highway_length", HighwayLength);
            json.WriteNumber("note_speed", NoteSpeed);

            json.WriteBoolean("can_tilt_activate", CanTiltActivate);
            json.WriteBoolean("lefty_flip", LeftyFlip);
            json.WriteBoolean("gamepad_mode", GamepadMode);
            json.WriteBoolean("is_bot", IsBot);
            json.WriteBoolean("show_name", ShowName);
            json.WriteBoolean("is_guest_profile", IsGuestProfile);
            json.WriteBoolean("is_remote_player", IsRemotePlayer);
        }
    }
}