using System;

namespace ChWebSocketClient
{
    public enum ControllerType : byte
    {
        Guitar,
        GHLGuitar,
        Drums,
        FiveLaneDrums,
    }

    public enum Instrument : sbyte
    {
        None = -1,
        Guitar,
        Bass,
        Rhythm,
        GuitarCoop,
        GHLGuitar,
        GHLBass,
        Drums,
        Keys,
        Band,
        ProDrums,
    }

    public enum Difficulty : sbyte
    {
        Easy,
        Medium,
        Hard,
        Expert,
    }

    [Flags]
    public enum Modifier
    {
        Unknown = 0,
        None = 1,
        AllStrums = 2,
        AllHOPOs = 4,
        AllTaps = 8,
        AllOpens = 16,
        MirrorMode = 32,
        Shuffle = 64,
        HOPOsToTaps = 128,
        LightsOut = 256,
        ModFull = 512,
        ModLite = 1024,
        ModPrep = 2048,
        PrecisionMode = 4096,
        DrunkMode = 8192,
        AutoStrum = 16384,
        BrutalMode = 32768,
        DroplessSustains = 65536,
        StrumlessHopos = 131072,
        DoubleNotes = 262144,
        NoFretGhosting = 524288,
        DoubleKick = 2,
        NoKick = 4,
        KickOnly = 8,
        DeadlyDynamics = 16
    }

    public enum GameMode
    {
        None = -1,
        Quickplay,
        Practice,
        Versus,
    }
}