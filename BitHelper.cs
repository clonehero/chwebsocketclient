using System.Collections.Generic;

namespace ChWebSocketClient
{
    public class BitHelper
    {

        public struct BitFlagEnumerator
        {
            private uint bits;
            private uint lastBits;

            public BitFlagEnumerator(uint bits)
            {
                this.bits = bits;
                lastBits = bits;
            }

            public bool MoveNext()
            {
                // This implementation uses a trick where subtracting 1 from any bitwise number
                // causes all bits below the least significant active bit to be flipped to 1
                // When bitwise and with the original value this masks out the least significant bit

                // Then finally when we go to get the current value we bitwise or with the
                // previous and current value to get the least significant bit 

                lastBits = bits;
                bits &= bits - 1U;

                if (bits != 0 || lastBits != 0)
                {
                    return true;
                }
                return false;
            }

            public uint Current => bits ^ lastBits;
            public BitFlagEnumerator GetEnumerator() => this;
        }

        /// <summary>
        /// Enumerate bit flags in uint value
        /// This is optimized for zero memory allocation and to be as fast as possible
        /// </summary>
        /// <param name="bits">Bits to enumerate</param>
        /// <returns>A <see cref="BitFlagEnumerator" /></returns>
        public static BitFlagEnumerator EnumerateBitFlags(uint bits) => new BitFlagEnumerator(bits);
    }
}