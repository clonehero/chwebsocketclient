﻿using System;
using System.Text.Json;

namespace ChWebSocketClient
{
    public interface IEvent
    {
        EventTypes EventType { get; }
        void DeserializeJSON(ref Utf8JsonReader json);
        void SerializeJSON(Utf8JsonWriter json);
    }
}